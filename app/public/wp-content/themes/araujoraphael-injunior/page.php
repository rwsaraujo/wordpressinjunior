<?php get_header() ?>

        <main>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); $pageHome = get_page_by_title( 'Home Page' ); ?>
                <p><?php the_field('paragrafo', $pageHome) ?></p>
        </main>

<?php get_footer() ?>
