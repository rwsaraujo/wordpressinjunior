<?php
    /* 
        Template Name: Posts
    */
?>

<?php get_header() ?>

        <main>
            <h1>Home.php</h1>
            <h2>Posts padrão</h2>
            <?php
                        get_search_form();
                        if ( have_posts() ) : 
                            while ( have_posts() ) : the_post(); ?>
                                <h3><?php the_title(); ?></h3>
                                <!--<p> <?php // the_content(); ?></p> -->
                                <a href="<?php the_permalink(); ?>">Link</a>

            <?php           endwhile; 
                        else : ?>
                            <p><?php esc_html_e( ' empty' ); ?></p>
            <?php 
                        endif; 
                        echo paginate_links();
            ?>
            <h2>Custom Posts</h2>
            <?php
                $args = array (
                    'paged_per_page' => 1,
                    'post_type' => 'noticia',
                    'post_status' => 'publish',
                    'suppress_filters' => true,
                    'orderby' => 'post_date',
                    'order' => 'DESC'
                );
                $news = new WP_Query($args);

                        // get_search_form();
                        if ($news -> have_posts() ) : 
                            while ($news -> have_posts() ) : $news -> the_post(); ?>
                                <h3><?php the_title(); ?></h3>
                                <!--<p> <?php // the_content(); ?></p> -->
                                <a href="<?php the_permalink(); ?>">Link</a>

            <?php           endwhile; 
                        else : ?>
                            <p><?php esc_html_e( 'empty' ); ?></p>
            <?php 
                        endif; 
                        $big = 99999999999;
                        echo paginate_links(
                            array(
                                'base' => str_replace($big, '%#%', esc_url( get_pagenum_link($big))),
                                'format' => '?paged=%#%',
                                'current' => max(1, get_query_var( 'paged' )),
                                'total' => $news->max_num_pages
                            )
                        );
            wp_reset_postdata(  );
            ?>
        </main>

<?php get_footer() ?>
