<?php get_header() ?>

        <main>
            <h1>search.php</h1>
            <?php get_template_part( 'inc', 'section', array('key' => 'Pesquisa pesquisa pesquisa')); ?>
            <?php
                        get_search_form();
                        if ( have_posts() ) : 
                            while ( have_posts() ) : the_post(); ?>
                                <h2><?php the_title(); ?></h2>
                                <!--<p> <?php // the_content(); ?></p> -->
                                <a href="<?php the_permalink(); ?>">Link</a>

            <?php           endwhile; 
                        else : ?>
                            <p><?php esc_html_e( 'Não temos posts' ); ?></p>
            <?php 
                        endif; 
                        echo paginate_links();
            ?>
        </main>

<?php get_footer() ?>
