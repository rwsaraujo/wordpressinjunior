<?php
    /* 
        Template Name: Home Page
    */
?>

<?php get_header() ?>

        <main>
            <h1><?php the_title() ?></h1>
            <?php 
                echo get_the_post_thumbnail();
                the_content()
            ?>
        </main>

<?php get_footer() ?>
