<?php
    /* 
        Template Name: Home Page
    */
?>

<?php get_header() ?>

        <main>
            <h1><?php the_title(); ?></h1>
            <?php get_template_part( 'inc', 'section', array('key' => 'Texto texto texto')); ?>
            <?php the_field('paragrafo'); ?></p>
            <?php $image = (get_field('imagem')); ?>
            <img src="<?php echo $image['url'] ?>" alt="">

        </main>

<?php get_footer() ?>
