<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AYYVr0p6lDTCGAsFm9xrUM8BKX/ag2e+qvsA1/24wAH8wCENNIAJiwOB0hnxGTLUk6OKdQ9gznmtYQeC/SRKHQ==');
define('SECURE_AUTH_KEY',  'Psq+iIeZquIWPBfxUDN+Lex4FzMTD0sQ/Gf+VNTOZst1lDkDjrfYhwF8R/pTIE4KOacUtSERVuOXRtBXyP9+bw==');
define('LOGGED_IN_KEY',    'ZGvEVA3DUOaRJE5IJahlKZKAtx7yJ4zxVG/m//YrvpqWnb7YYUm8gHjMtqmJZtZtv5SrJVDyIETxwaT78rlgvA==');
define('NONCE_KEY',        'zkUA4gsCKqNtUbaenUQvIqYoewTjVByvDuBvMvAvWLRJS1BDO2H6MozQRYOzZFUauUaPk2RLVCFzICKbylwYgw==');
define('AUTH_SALT',        'oUSWGwwaXiLyHAHhzaSMghVTWpHJQvSXZML06JGXZqJA+M+82BXatG57upXsTJXKPdYzsReOrONTafPG/3Fa6A==');
define('SECURE_AUTH_SALT', '9aeCAXY5zYpOCP2w7LtdpJSPuyI2BcneuXIk4G/cRzImqRPMZsW+FbuMO6GvjXqCoINZx9buYrux64RKWJh8RQ==');
define('LOGGED_IN_SALT',   'JbnvF9ODJGPQ+i26paxZV0IxXaLu/IDwfRxbTzapApnCUruYSJlxwTikjXF7m2hRZ7jjGESpyu0RvC3r4aOEwg==');
define('NONCE_SALT',       'EhOajHUwV2zpOvtZ6iePKEjXtoddonuH6i62XdhbwCh/AaRfKXWJaUU3HXk8SSUGAgAULPxuZwPFb6KWEpQp4A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
